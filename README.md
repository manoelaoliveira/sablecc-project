# README #

Projeto de desenvolvimento da linguagem Mongo, uma variação da linguagem Monga, da matéria Linguagens Formais e Tradutores, Curso Sistemas de Informação - Universidade Federal de Sergipe - UFS - 2015.



### Para que serve o projeto? ###

O projeto tem como objetivo avaliar os alunos desenvolvedores:

	- Ademir Almeida da Costa Júnior
	- Manoela dos Reis Oliveira
	- Samir Dantas Oliveira

Orientadora: Porfª Drª Beatriz Trinchão Andrade de Carvalho

### Com funciona o programa? ###

Para rodar a parte léxica da linguagem, é necessário clonar o repositório e executar o arquivo "Main", que irá imprimir os tokens da especificação da linguagem Mongo escritos, por escolha do usuário, no arquivo "tester.mongo".

### Suporte ###

Em caso de dúvidas ou sugestões, favor contactar algum dos desenvolvedores.