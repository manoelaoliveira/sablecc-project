package main;

import java.io.IOException;

import lexico.*;

// classe principal para teste, usando o arquivo tester.mongo
public class Main {	

	public static void main(String[] args) throws IOException {

		LexTest lexT = new LexTest();
		lexT.printTokens("tester.mongo");
		
	}
}