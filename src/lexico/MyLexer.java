package lexico;

import java.io.PushbackReader;

import grammar.lexer.*;
import grammar.node.EOF;
import grammar.node.TAsterisk;
import grammar.node.TCommentblock;
import grammar.node.TCommentfinal;
import grammar.node.TDiv;
import grammar.node.TTimes;

public class MyLexer extends Lexer {

	public MyLexer(PushbackReader in) {
		super(in);
		// TODO Auto-generated constructor stub
	}

	private int count;
	private int countFinal;
	private TCommentblock comment;
	private StringBuffer text;

	protected void filter() throws LexerException {
	
		if (state.equals(State.COMMENT)) {
			// se entrou no estado
			if (comment == null) {
				// O tken deve
				// We keep a reference to it and set the count to one
				if (token instanceof TCommentfinal) {
					//throw new LexerException(null, "error block comment");
					System.out.print("["+token.getLine()+","+token.getPos()+"] error block comment ");
					state = State.NORMAL;
					token = null;
					
				} else {
					comment = (TCommentblock) token;
					text = new StringBuffer(comment.getText());
					count = 1;
					token = null; // continue to scan the input.
				}
				
			} else {
				// we were already in the comment state
				text.append(token.getText()); // accumulate the text.
				if (token instanceof TCommentblock) {
					count++;
				} else if (token instanceof TCommentfinal) {
					count--;
				}
				if (token instanceof EOF) {
					 throw new LexerException(null, "["+token.getLine()+","+token.getPos()+"] error block comment ");
				}
				if (count != 0)
					token = null; // continue to scan the input.
				else {
					// comment.setText(text.toString());
					token = comment; // return a comment with the full text.
					state = State.NORMAL; // go back to normal.
					comment = null; // We release this reference.
				}
			}
		}
	}

}
