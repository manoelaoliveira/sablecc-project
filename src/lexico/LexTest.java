package lexico;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PushbackReader;

import lexico.MyLexer;
import grammar.node.EOF;
import grammar.node.TEmpty;
import grammar.node.Token;

// classe que irar ler token por token usando uma fila	

public class LexTest {

	public void printTokens(String file) throws IOException {

			
		try {
			MyLexer lex = new MyLexer(new PushbackReader(new FileReader(file), 500));
			
			int linha = 0;
			lex.toString();
			// ler ate o final do arquivo
			while (!(lex.peek() instanceof EOF)) {
				
				if(lex.peek().getLine() > linha){
					linha = lex.peek().getLine();
				}
				Token t = lex.next();
				if(t instanceof TEmpty){
					System.out.print(t.getText());
				}else{
					System.out.print(t.getClass().getSimpleName());
				}
				
			}
			
		}
		// erro cao nao encontre o arquivo
		catch (FileNotFoundException ex) {
			System.out.println("Arquivo no encontrado!");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
